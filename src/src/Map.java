package src;

import java.io.Serializable;
import java.util.ArrayList;

public class Map implements Serializable {

  private int id;
  private ArrayList<CoordinatesHandler> navigationCoordinates;

  public Map(int id, ArrayList<CoordinatesHandler> navigationCoordinates) {
    this.id = id;
    this.navigationCoordinates = navigationCoordinates;
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public ArrayList<CoordinatesHandler> getNavigationCoordinates() {
    return this.navigationCoordinates;
  }

  public void setNavigationCoordinates(
    ArrayList<CoordinatesHandler> navigationCoordinates
  ) {
    this.navigationCoordinates = navigationCoordinates;
  }

  @Override
  public String toString() {
    String string = "Id: " + this.id + "\nNavigation Coordinate List:\n";

    for (CoordinatesHandler navigationCoordinate : navigationCoordinates) {
      string += "-> " + navigationCoordinate.toString() + "\n";
    }

    return string;
  }
}
